//package iot.home.smart_garden;
//
//import io.vertx.core.AbstractVerticle;
//import io.vertx.core.Vertx;
//import iot.home.smart_garden.serial.CommChannel;
//import iot.home.smart_garden.serial.SerialCommChannel;
//
//import java.util.concurrent.TimeUnit;
//
//public class RunService extends AbstractVerticle {
//
//  public static void main(String...strings) throws Exception {
//    Vertx vertx = Vertx.vertx();
//    String serialPort = "/dev/ttyACM0"; // Assumed to never change
//    CommChannel channel = new SerialCommChannel(serialPort, 9600);
//    MQTTSerialClientVerticle client = new MQTTSerialClientVerticle();
//
//    vertx.deployVerticle(client);
//    vertx.setPeriodic(TimeUnit.SECONDS.toMillis(1000), l -> {
//      try {
////        client.parseMsg();
//      } catch (InterruptedException e) {
//        e.printStackTrace();
//      }
//    });
//  }
//}
