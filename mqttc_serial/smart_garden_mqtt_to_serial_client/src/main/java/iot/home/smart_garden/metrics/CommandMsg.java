package iot.home.smart_garden.metrics;

public class CommandMsg {
  private String topic;
  private String value;

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public String getTopic() {
    return topic;
  }

  public String getValue() {
    return value;
  }

  public String asJsonString() {
    return "{\"value\":\"" + value + "\"}";
  }
}
