package iot.home.smart_garden.serial;

public class MqttMsg {

  String topic;
  String name;
  String value;

  public String getTopic() {
    return topic;
  }

  public String getName() {
    return name;
  }

  public String getValue() {
    return value;
  }
}
