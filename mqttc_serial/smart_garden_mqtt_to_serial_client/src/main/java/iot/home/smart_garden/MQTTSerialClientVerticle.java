package iot.home.smart_garden;

import com.google.gson.Gson;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.mqtt.MqttClient;
import iot.home.smart_garden.metrics.CommandMsg;
import iot.home.smart_garden.metrics.Message;
import iot.home.smart_garden.serial.CommChannel;
import iot.home.smart_garden.serial.SerialCommChannel;
import jssc.SerialPortException;

import static iot.home.smart_garden.common.Topics.*;

public class MQTTSerialClientVerticle extends AbstractVerticle {

  private final Gson g = new Gson();
  private final CommChannel channel;
  private MqttClient client;

  public MQTTSerialClientVerticle(CommChannel channel) {
    this.channel = channel;
  }

  @Override
  public void start() throws SerialPortException {
    String broker = "mosquitto"; // Work thanks to the DNS service of Docker
    client = MqttClient.create(vertx);
    String serialPort = "/dev/ttyACM0"; // Assumed to never change
    channel.init(serialPort, 9600);
    client.connect(1883, broker, con -> {
      client.publishHandler(s -> {
        String topic = s.topicName();
        String payload = s.payload().toString();
        Message msgIn = parse(payload, Message.class);
        msgIn.setTopic(topic);
        System.out.println("IN " + msgIn.asJsonString());
        channel.sendMsg(msgIn.asJsonString());
      });
      client.subscribe(state.getText(), 0);
    });
  }

  public void parseMsg() throws InterruptedException {
    if(channel.isMsgAvailable()) {
      String receivedData = channel.receiveMsg();
      System.out.println("OUT " + receivedData);
      if(!receivedData.contains("$")) {
        CommandMsg doc = parse(receivedData, CommandMsg.class);
        client.publish(
          doc.getTopic(),
          Buffer.buffer(doc.asJsonString()),
          MqttQoS.AT_MOST_ONCE,
          false,
          false
        );
      }
    }
  }

  private <T> T parse(String jsonDoc, Class<T> inputClass) {
    return g.fromJson(jsonDoc, inputClass);
  }

  public static void main(String...strings) {
    Vertx vertx = Vertx.vertx();
    CommChannel channel = new SerialCommChannel();
    MQTTSerialClientVerticle client = new MQTTSerialClientVerticle(channel);

    vertx.deployVerticle(client);
    vertx.setPeriodic(1000, l -> {
      try {
        client.parseMsg();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    });
  }
}
