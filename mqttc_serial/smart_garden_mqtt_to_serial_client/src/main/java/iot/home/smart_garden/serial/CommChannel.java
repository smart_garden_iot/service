package iot.home.smart_garden.serial;

import jssc.SerialPortException;

public interface CommChannel {

  /**
   * Send a message represented by a string (without new line).
   * <p>
   * Asynchronous model.
   *
   * @param msg message to be sent
   */
  void sendMsg(String msg);

  /**
   * To receive a message.
   * <p>
   * Blocking behaviour.
   */
  String receiveMsg() throws InterruptedException;

  /**
   * To check if a message is available.
   *
   * @return whether a message is available
   */
  boolean isMsgAvailable();

  /**
   * Should be called when the serial port is no longer needed to avoid port
   * locking.
   */
  void close();

  void init(String serialPort, int i) throws SerialPortException;
}
