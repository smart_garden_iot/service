package iot.home.smart_garden.serial;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.buffer.Buffer;
import io.vertx.mqtt.MqttClient;
import jssc.*;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class SerialCommChannel implements CommChannel, SerialPortEventListener {

  private SerialPort serialPort;
  private BlockingQueue<String> queue;
  private StringBuffer currentMsg = new StringBuffer();

  public SerialCommChannel() {}

  public void init(String port, int baudRate) throws SerialPortException {
    queue = new ArrayBlockingQueue<>(100);
    serialPort = new SerialPort(port);
    serialPort.openPort();

    serialPort.setParams(baudRate,
      SerialPort.DATABITS_8,
      SerialPort.STOPBITS_1,
      SerialPort.PARITY_NONE);

    serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN |
      SerialPort.FLOWCONTROL_RTSCTS_OUT);

    serialPort.addEventListener(this);
  }

  @Override
  public void sendMsg(String msg) {
    char[] array = (msg + "\n").toCharArray();
    byte[] bytes = new byte[array.length];
    for (int i = 0; i < array.length; i++) {
      bytes[i] = (byte) array[i];
    }
    try {
      synchronized (serialPort) {
        serialPort.writeBytes(bytes);
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  @Override
  public String receiveMsg() throws InterruptedException {
    return queue.take();
  }

  @Override
  public boolean isMsgAvailable() {
    return !queue.isEmpty();
  }

  @Override
  public void close() {
    try {
      if (serialPort != null) {
        serialPort.removeEventListener();
        serialPort.closePort();
      }
    } catch (Exception ex) {
      ex.printStackTrace();
    }
  }

  public void serialEvent(SerialPortEvent event) {
    /* if there are bytes received in the input buffer */
    if (event.isRXCHAR()) {
      try {
        String msg = serialPort.readString(event.getEventValue());
        msg = msg.replaceAll("\r", "");
        currentMsg.append(msg);
        boolean goAhead = true;

        while(goAhead) {
          String msg2 = currentMsg.toString();
          int index = msg2.indexOf("\n");
          if (index >= 0) {
            queue.put(msg2.substring(0, index));
            currentMsg = new StringBuffer();
            if (index + 1 < msg2.length()) {
              currentMsg.append(msg2.substring(index + 1));
            }
          } else {
            goAhead = false;
          }
        }

      } catch (Exception ex) {
        ex.printStackTrace();
        System.out.println("Error in receiving string from COM-port: " + ex);
      }
    }
  }
}
