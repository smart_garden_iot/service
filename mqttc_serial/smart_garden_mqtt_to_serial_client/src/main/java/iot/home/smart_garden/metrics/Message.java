package iot.home.smart_garden.metrics;

public class Message {
  private long timestamp;
  private String name;
  private int value;
  private String topic;

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public String getTopic() {
    return topic;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public String getName() {
    return name;
  }

  public int getValue() {
    return value;
  }

  public String asJsonString() {
    return "{\"topic\":\"" + topic +  "\",\"value\":\"" + value + "\"}";
  }
}
