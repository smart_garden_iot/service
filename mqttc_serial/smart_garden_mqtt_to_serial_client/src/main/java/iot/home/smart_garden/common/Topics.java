package iot.home.smart_garden.common;

public enum Topics {
  tankLevel("system/tankLevel"),
  telemetry("system/telemetry"),
  state("system/state"),
  ruleset("dashboard/ruleset");

  private final String text;

  Topics(String text) {
    this.text = text;
  }

  public String getText() {
    return this.text;
  }
}
