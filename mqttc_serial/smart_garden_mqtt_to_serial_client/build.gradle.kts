import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.gradle.api.tasks.testing.logging.TestLogEvent.*

plugins {
    java
    application
    id("com.github.johnrengelman.shadow") version "7.0.0"
}

group = "iot.home"
version = "1.0.0-SNAPSHOT"

repositories {
    mavenCentral()
}

val mainVerticleName = "iot.home.smart_garden.MQTTSerialClientVerticle"
val launcherClassName = "io.vertx.core.Launcher"

val watchForChange = "src/**/*"
val doOnChange = "${projectDir}/gradlew classes"

application {
    mainClass.set(mainVerticleName)
}

dependencies {
    implementation(platform("io.vertx:vertx-stack-depchain:4.1.2"))
    implementation("io.vertx:vertx-mqtt:4.1.2")
    testImplementation("io.vertx:vertx-junit5:4.1.2")
    testImplementation("org.junit.jupiter:junit-jupiter:5.7.2")
    implementation("com.fazecast:jSerialComm:2.7.0")
    implementation("org.scream3r:jssc:2.8.0")
    implementation("com.google.code.gson:gson:2.8.5")
}

java {
    sourceCompatibility = JavaVersion.VERSION_16
    targetCompatibility = JavaVersion.VERSION_16
}

tasks.withType<ShadowJar> {
    archiveClassifier.set("fat")
    manifest {
        attributes(mapOf("Main-Verticle" to mainVerticleName))
    }
    mergeServiceFiles()
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        events = setOf(PASSED, SKIPPED, FAILED)
    }
}

tasks.withType<JavaExec> {
    args = listOf("run", mainVerticleName, "--redeploy=$watchForChange", "--launcher-class=$mainVerticleName", "--on-redeploy=$doOnChange")
}
