package iot.home.smart_garden.common;

public enum Topics {
  tankLevel("system/tankLevel"),
  telemetry("system/telemetry"),
  state("system/state"),
  wateringState("system/wateringState");

  private final String text;

  Topics(String text) {
    this.text = text;
  }

  public String getText() {
    return this.text;
  }
}
