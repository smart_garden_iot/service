package iot.home.smart_garden.database;

import iot.home.smart_garden.common.Topics;

public interface DBWriter {
  void connect();
  void disconnect();
  void writeTelemetryData(String jsonDoc, Topics topic);
  void writeStateData(String jsonDoc, Topics topic);
}
