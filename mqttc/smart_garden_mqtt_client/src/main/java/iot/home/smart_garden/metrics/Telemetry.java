package iot.home.smart_garden.metrics;

public class Telemetry {
  private long timestamp;
  private Metric[] metrics;

  public long getTimestamp() {
    return timestamp;
  }

  public Metric[] getMetrics() {
    return metrics;
  }
}
