package iot.home.smart_garden.metrics;

import com.google.gson.annotations.Expose;

public class State {
  private long timestamp;
  private String name;
  private int value;
  private String reason;

  public long getTimestamp() {
    return timestamp;
  }

  public String getName() {
    return name;
  }

  public int getState() {
    return value;
  }

  public String getReason() {
    return reason;
  }
}
